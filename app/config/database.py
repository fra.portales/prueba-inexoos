from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from flask import Flask

import configparser
import os
import ast




atApiConfig = configparser.RawConfigParser()

#Get DB Data from db.ini file
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
atApiConfig.read(os.path.join(BASE_DIR, 'config/db.ini'))
#Set global variables
DBNAME = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'db_name'))
USER = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'user'))
PASSWORD = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'password'))
HOST = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'host'))
PORT = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'port'))

engine = create_engine('mysql://'+USER+':'+PASSWORD+'@'+HOST+':'+PORT+'/'+DBNAME, connect_args={'connect_timeout': 1000},convert_unicode=True,pool_size=20, max_overflow=0)
sqla_session = sessionmaker(bind=engine)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

# connection = engine.connect()
# result = connection.execute("select * from hospitales.tipo_consulta")
# for row in result:
#     print("username:", row['nombre'])
# connection.close()
                                
Base = declarative_base()
Base.query = db_session.query_property()

def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    import app.common.models.public
    Base.metadata.create_all(bind=engine)
