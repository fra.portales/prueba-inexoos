from flask import Flask
import os
import configparser
import ast

atApiConfig = configparser.RawConfigParser()


#Get DB Data from db.ini file
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
atApiConfig.read(os.path.join(BASE_DIR, 'config/db.ini'))
#Set global variables
DBNAME = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'db_name'))
USER = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'user'))
PASSWORD = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'password'))
HOST = ast.literal_eval(atApiConfig.get('DB_CONFIG', 'host'))


# Trae DB info del archivo db.ini
atApiConfig.read(os.path.join(BASE_DIR, 'config/db.ini'))


TEMPLATES = [
    {
        'DIRS': [os.path.join(BASE_DIR,'public/template')]
    },
]