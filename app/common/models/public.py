# -*- coding: utf-8 -*-
from flask import Flask
from flask_sqlalchemy import SQLAlchemy 
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Numeric
from app.config.database import Base
from app import app

"""
@class Hospital
@brief Modelo representativo de la tabla hospital
"""
class Hospital(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'hospital'

    id      = Column(Integer, primary_key=True)
    nombre  = Column(String(50), unique=True)
    
    class Meta:
        managed = False
        db_table = 'hospitales.consulta'

    def __repr__(self):
        return f"{self.id}, {self.nombre}"
    def get_id(self):
        return self.id


"""
@class Consulta
@brief Modelo representativo de la tabla consulta
"""
class Consulta(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'consulta'

    id                  = Column(Integer, primary_key=True)
    nombreEspecialista  = Column(String(50), unique=True)
    idTipoConsulta      = Column(Integer,default=0)
    cant_pacientes      = Column(Integer,default=0)
    idHospital          = Column(Integer)
    idEstado            = Column(Integer,ForeignKey('Estados.id'))

    class Meta:
        managed = False
        db_table = 'hospitales.consulta'

    def __repr__(self):
        return f"{self.id}, {self.nombreEspecialista}, {self.cant_pacientes}"
    def get_id(self):
        return self.id

    
"""
@class TipoConsulta
@brief Modelo representativo de la tabla tipo_consulta
"""
class TipoConsulta(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'tipo_consulta'

    id      = Column(Integer, primary_key=True)
    nombre  = Column(String(50), unique=True)
    status  = Column(Integer,default=1)

    class Meta:
        managed = False
        db_table = 'hospitales.tipo_consulta'

    def __repr__(self):
        return f"'{self.id}', '{self.nombre}'"
    def get_id(self):
        return self.id
"""
@class Estados
@brief Modelo representativo de la tabla estados
"""
class Estados(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'estados'

    id          = Column(Integer, primary_key=True)
    descripcion = Column(String(50), unique=True)
    
    class Meta:
        managed = False
        db_table = 'hospitales.estados'

    def __repr__(self):
        return f"{self.id}, {self.descripcion}"
    def get_id(self):
        return self.id

"""
@class Paciente
@brief Modelo representativo de la tabla hospitales.paciente
"""
class Paciente(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'paciente'
    
    id                  = Column(Integer , primary_key=True)
    nombre              =  Column(String(50), unique=True)
    apellido            =  Column(String(50), unique=True)
    edad                = Column(Integer)
    noHistoriaClinica   = Column(String(50), unique=True)
    
    class Meta:
        managed = False
        db_table = 'hospitales.paciente'

    def __repr__(self):
        return f"{self.id}, {self.nombre}, {self.edad},{self.noHistoriaClinica}"
    def get_id(self):
        return self.id
    
"""
@class DetallePaciente
@brief Modelo representativo de la tabla hospitales.detalle_paciente
"""
class DetallePaciente(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'detalle_paciente'

    id              = Column(Integer , primary_key=True)
    pesoEstatura    = Column(DateTime)
    tieneDieta      = Column(Integer)
    idPaciente      = Column(Integer)
    fumador         = Column(Integer)
    riesgo          = Column(Numeric)
    prioridad       = Column(Numeric)
    status          = Column(Integer)
    
    class Meta:
        managed = False
        db_table = 'hospitales.detalle_paciente'

    def __repr__(self):
        return f"{self.id},{self.pesoEstatura},{self.tieneDieta},{self.idPaciente},{self.fumador},{self.status}"
    def get_id(self):
        return self.id

"""
@class Sala
@brief Modelo representativo de la tabla hospitales.sala
"""
class Sala(Base):
    __table_args__ = {'schema':'hospitales'}
    __tablename__ = 'sala'

    id          = Column(Integer , primary_key=True , autoincrement=True)
    fecha       = Column(DateTime)
    idConsulta  = Column(Integer)
    idPaciente  = Column(Integer)
    idEstado    = Column(Integer)
    status      = Column(Integer)
    class Meta:
        managed = False
        db_table = 'hospitales.sala'

    def __repr__(self):
        return f"{self.id},{self.fecha},{self.idConsulta},{self.idPaciente},{self.idEstado}"
    def get_id(self):
        return self.id
    
