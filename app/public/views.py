from app.common.models import *
from app.config.database import db_session
from datetime import datetime
from sqlalchemy import func, null,desc,or_



def cleanAllSalas():
    s       = Sala

    result = s.query.filter(s.status == 1).all()
    for row in result:
        row.status = 0
        db_session.commit()

    return 'ok'

def getPacientesSinAtender():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    e       = Estados
    columns = ['Nombre','Apellido','Edad','N° Historia Clinica','Fumador','Dieta','Peso-Estatura']

    result = p.query\
            .outerjoin(s,s.idPaciente==p.id,isouter=True,full = False)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .outerjoin(e,e.id==s.idEstado,isouter=True,full = False)\
            .with_entities(p.id,p.nombre,p.apellido,p.edad,p.noHistoriaClinica,dp.tieneDieta,dp.fumador,dp.pesoEstatura,dp.riesgo,dp.prioridad,e.descripcion)\
            .filter(or_(s.id==None,s.status==1),e.id==None)\
            .all()

    data = crearEstructura(result)

    return {'columnas':columns,'data': data}



def getPacientesUrgencias():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    e       = Estados

    columns = ['Nombre','Apellido','Edad','N° Historia Clinica','Fumador','Dieta','Peso-Estatura']

    result = p.query\
            .outerjoin(s,s.idPaciente==p.id)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .join(e,e.id==s.idEstado)\
            .with_entities(p.id,p.nombre,p.apellido,p.edad,p.noHistoriaClinica,dp.tieneDieta,dp.fumador,dp.pesoEstatura,dp.riesgo,dp.prioridad,e.descripcion)\
            .filter(s.idConsulta==6,s.status==1)\
            .all()
            
    data = crearEstructura(result)

    return {'columnas':columns,'data': data}

def getPacientesUrgenciasCgi():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    e       = Estados

    columns = ['Nombre','Apellido','Edad','N° Historia Clinica','Fumador','Dieta','Peso-Estatura']

    result = p.query\
            .outerjoin(s,s.idPaciente==p.id)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .join(e,e.id==s.idEstado)\
            .with_entities(p.id,p.nombre,p.apellido,p.edad,p.noHistoriaClinica,dp.tieneDieta,dp.fumador,dp.pesoEstatura,dp.riesgo,dp.prioridad,e.descripcion)\
            .filter(s.idConsulta==5,s.status==1)\
            .all()
            
    data = crearEstructura(result)

    return {'columnas':columns,'data': data}

def getPacientesPediatria():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    e       = Estados

    columns = ['Nombre','Apellido','Edad','N° Historia Clinica','Fumador','Dieta','Peso-Estatura']

    result = p.query\
            .outerjoin(s,s.idPaciente==p.id)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .join(e,e.id==s.idEstado)\
            .with_entities(p.id,p.nombre,p.apellido,p.edad,p.noHistoriaClinica,dp.tieneDieta,dp.fumador,dp.pesoEstatura,dp.riesgo,dp.prioridad,e.descripcion)\
            .filter(s.idConsulta==4,s.status==1)\
            .all()
            
    data = crearEstructura(result)

    return {'columnas':columns,'data': data}

def crearEstructura(result):

    l = []
    for x in result:
        python_obj = {
                "nombre":x.nombre,
                "apellido":x.apellido,
                "edad":x.edad,
                "noHistoriaClinica":x.noHistoriaClinica,
                "fumador":x.fumador,
                "dieta":x.tieneDieta,
                "pesoEstatura":x.pesoEstatura,
                "id": x.id,
                "riesgo":x.riesgo,
                "prioridad":x.prioridad,
                "Estado":x.descripcion
                }
        l.append(python_obj)
    
    return l




def updateDPaciente(detalles):
    
    for detalle in detalles:
        dp   = DetallePaciente
        result = dp.query.filter(dp.idPaciente == detalle['id'])
        for row in result:        
            if row.id >0:
                row.riesgo = detalle['riesgo']
                row.prioridad = detalle['prioridad']
                db_session.commit()
    
    return 'ok'

def llenarSalaUrgencia():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    c       = Consulta 

    salaUrgencia = c.query.filter(c.idTipoConsulta == 2).first()
    
    result = p.query\
            .outerjoin(s,s.idPaciente==p.id,isouter=True,full = False)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .with_entities(p.id,dp.riesgo,dp.prioridad)\
            .filter(or_(s.id==None,s.status==1),dp.prioridad>4)\
            .order_by(desc(dp.prioridad))\
            .limit(salaUrgencia.cant_pacientes).all()
    
    for row2 in result:
        s       = Sala()

        s.fecha = datetime.now()
        s.idConsulta    = salaUrgencia.id
        s.idEstado      = 1
        s.idPaciente    = row2.id
        s.status        = 1
        db_session.add(s)   
        db_session.commit()

    return 'ok'


def completarSalaCgi():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    c       = Consulta 

    salaCgi = c.query.filter(c.idTipoConsulta == 3).first()
    
    result = p.query\
            .outerjoin(s,s.idPaciente==p.id,isouter=True,full = False)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .with_entities(p.id,dp.riesgo,dp.prioridad)\
            .filter(or_(s.id==None,s.status==1),p.edad>15,dp.prioridad<4)\
            .order_by(desc(dp.prioridad))\
            .limit(salaCgi.cant_pacientes).all()
    print(result)
    for row2 in result:
        s       = Sala()

        s.fecha = datetime.now()
        s.idConsulta    = salaCgi.id
        s.idEstado      = 1
        s.idPaciente    = row2.id
        s.status        = 1
        db_session.add(s)   
        db_session.commit()

    return getPacientesUrgenciasCgi()



def completarSalaPediatria():
    p       = Paciente
    s       = Sala
    dp      = DetallePaciente
    c       = Consulta 

    salaCgi = c.query.filter(c.idTipoConsulta == 1).first()
    
    result = p.query\
            .outerjoin(s,s.idPaciente==p.id,isouter=True,full = False)\
            .outerjoin(dp,dp.idPaciente==p.id,isouter=True,full = False)\
            .with_entities(p.id,dp.riesgo,dp.prioridad)\
            .filter(or_(s.id==None,s.status==1),p.edad<15,dp.prioridad<=4)\
            .order_by(desc(dp.prioridad))\
            .limit(salaCgi.cant_pacientes).all()
    for row2 in result:
        s       = Sala()

        s.fecha = datetime.now()
        s.idConsulta    = salaCgi.id
        s.idEstado      = 1
        s.idPaciente    = row2.id
        s.status        = 1
        db_session.add(s)   
        db_session.commit()

    return getPacientesUrgenciasCgi()

