from app import app
from app.public.views import *
from flask import Flask, render_template,  url_for, flash, redirect, request, jsonify, session
from datetime import timedelta


@app.before_request
def before_request():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=15)

@app.route("/") 
@app.route("/home")
def home():
    return render_template('index.html', title='Index')

@app.route("/salas", methods=['GET', 'POST'])
def salas():
    return render_template('salas.html')

@app.route("/atender", methods=['GET', 'POST'])
def atender():
    return render_template('atender.html', title='Login')


@app.route("/mayorRiesgo")
def mayorRiesgo():
    return redirect(url_for('paciente_mayor_riesgo.html'))

@app.route("/fumadores")
def fumadores():
    return render_template('pacientes_fumadores.html')






@app.route("/getListaPacientes", methods=['POST'])
def getListaPacientes():
    data = getPacientesSinAtender()
    return data



@app.route("/getUrgencias", methods=['POST'])
def getUrgencias():
    data = getPacientesUrgencias()
    return data

@app.route("/updateDetallePaciente", methods=['POST'])
def updateDetallePaciente():
    detalles = request.get_json(force = True)
    result = updateDPaciente(detalles)
    llenarSalaUrgencia()
    return result

@app.route("/getCgi", methods=['POST'])
def getCgi():
    result = getPacientesUrgenciasCgi()    
    return result

@app.route("/llenarSalaCgi", methods=['POST'])
def llenarSalaCgi():
    result = completarSalaCgi()
    return result
    
@app.route("/getPediatria", methods=['POST'])
def getPediatria():
    result = getPacientesPediatria()    
    return result

@app.route("/llenarSalaPediatria", methods=['POST'])
def llenarSalaPediatria():
    result = completarSalaPediatria()
    return result
    
    
@app.route("/cleanSalas", methods=['POST'])
def cleanSalas():
    result = cleanAllSalas()
    return result