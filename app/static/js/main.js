$(document).ready(function(){
    
});




function crearTabla(datos,id){
    let prioridad;
    let riesgo;
    $('#datatable-'+id).append('<tbody></tbody>');
    $('#datatable-'+id+' tbody').empty();
    $.each(datos,function(i,val){
        
        $('#datatable-'+id+' tbody').append('<tr class="grade_'+val.id+'" id="paciente_'+val.id+'">');

        td  = '<td>'+val.nombre+'</td>';
        td += '<td>'+val.apellido+'</td>';
        td += '<td>'+val.edad+'</td>';
        td += '<td>'+val.noHistoriaClinica+'</td>';
        td += '<td>'+val.fumador+'</td>';
        td += '<td>'+val.dieta+'</td>';
        td += '<td>'+val.pesoEstatura+'</td>';
        td += '<td>'+val.prioridad+'</td>';
        td += '<td>'+val.riesgo+'</td>';
        if ( id != 'details'){
            if (i == 0){
                td += '<td><input type="button" id="btgenerar" value="Atender"></td>';
            }else{
                td += '<td>'+val.Estado+'</td>';
            }
            
        }else{
            td += '<td><input type="button" id="btgenerar" value="Atender"></td>';
        }
        
        $('.grade_'+val.id).append(td);
            
    });
    
    
}

/**Funcion que genera la el thead de la tabla segun los parametros de la db**/
function crearFilaTitulo(titulos,id){
    
    $('#datatable-'+id+'').append('<thead><tr></tr></thead>');
    let th;
    $.each(titulos,function(index,columna){
        th += '<th style="width=70px;">'+columna+'</th>';
    });

    th += '<th style="width=70px;">Estado</th>';
    $('#datatable-'+id+' thead tr').append(th);
}


function agrgarCalculos(datos){
    $.each(datos,function(i,val){
        let edad            = val.edad;
        let fumador         = validarDatoNumerico(val.fumador);
        let dieta           = validarDatoNumerico(val.dieta);
        let pesoEstatura    = validarDatoNumerico(val.pesoEstatura);

        prioridad   =calcularPriodidad(edad,fumador,dieta,pesoEstatura);
        riesgo      = getRiesgo(val.edad,prioridad);
        
        val.fumador         = fumador;
        val.dieta           = dieta;
        val.pesoEstatura    = pesoEstatura;
        val.prioridad       = prioridad;
        val.riesgo          = riesgo;
    });
    return datos;
}
function calcularPriodidad(edad,fumador,dieta,pesoEstatura){

    let result;
    
    switch(true){
        case edad >= 1 && edad <= 15 :
            result = prioridadNinos(edad,pesoEstatura);
            break;
        case edad >= 16 && edad <= 40 :
            result = prioridadJovenes(fumador);
            break;
        case 41 <= edad :
            result = prioridadAncianos(edad,dieta);
            break;
        default:
            break;
    }

    return result;
}

function prioridadNinos(edad,pesoEstatura){
    let prd;
    switch(edad){
        case edad >= 1 && edad <= 5:
            prd = pesoEstatura+3;
            return prd;
        case edad >= 6 && edad <= 12:
            prd = pesoEstatura+2;
            return prd;
        default:
            prd = pesoEstatura+1;
            return prd;
    }
}

function prioridadJovenes(fumador){
    let prd;
    if (fumador > 0 )
    {
        prd = (fumador/4)+2;
        return prd;
    }else
    {
        prd = 2;
        return prd;
    }
}

function prioridadAncianos(edad,dieta){
    let prd;
    prd = (edad/30)+3;
    if( edad >= 60 && edad <= 100 ){
        prd = (edad/20)+4;
    }

    return prd;
}

function getRiesgo(edad,prioridad){
    let riesgo;
    
    if( edad < 40 )
    {
        riesgo = ((edad*prioridad)/100)+5.3;
    }else
    {
        riesgo = ((edad*prioridad)/100);
    }

    return riesgo;
}


function validarDatoNumerico(dato){
    if(dato == null || dato == undefined ){ 
        return 0;
    }
    return dato;
}



//ajax


function callPacientesSinAtender(){
    let data;
    $.ajax({
        method: 'POST',
        url: '../getListaPacientes',
        async: !1,
        success: function(response) {
            data = response;
        },
        done:function(response){
            data = response;
        },
        finally:function(response){
            data = response;
        },
    });
    return data;

}