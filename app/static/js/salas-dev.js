$(document).ready(function(){
    getSalaUrgencia();
    getSalaCGI();
    getSalaPediatria();



    $('#cleanSalas').click(function() {
        console.log('llego')
       
        $.ajax({
            method: 'POST',
            url: '../cleanSalas',
            success: function(response) {
                if(response != 0){
                    $('#datatable-urgencias').dataTable().fnClearTable();
                    $('#datatable-CGI').dataTable().fnClearTable();
                    $('#datatable-Pediatria').dataTable().fnClearTable();   
    
                }else{
                    
                }
                
                
            }
        });
    });
});




function getSalaUrgencia(){
    $('#datatable-urgencias thead').remove();
    $('#datatable-urgencias tbody').remove();
    
    $.ajax({
        method: 'POST',
        url: '../getUrgencias',
        success: function(response) {
            if(response.data == 0){
                response = callPacientesSinAtender();
                response['columnas'].push('Prioridad','Riesgo')
                datos = agrgarCalculos(response['data']);
                $.ajax({
                    method: 'POST',
                    url: '../updateDetallePaciente',
                    data:JSON.stringify(datos) ,
                    success: function(response) {
                        $('.panel-body').show();
                        crearFilaTitulo(response['columnas'],"urgencias");
                        crearTabla(response['data'],"urgencias");
                        
                    }
                });

            }else{
                datos= response['data'];
                $('.panel-body').show();
                response['columnas'].push('Prioridad','Riesgo')
                crearFilaTitulo(response['columnas'],"urgencias");
                crearTabla(response['data'],"urgencias");
                
            }
            $('#datatable-urgencias').dataTable({
                order: [ [7,'desc'],[8, 'desc']],
                pageLength: 10,
                bPaginate: false,
                bLengthChange: false,
                bFilter: false,
                bInfo: false,
                bAutoWidth: false
            } );
            
        }
    });

    
}



function getSalaCGI(){
    $('#datatable-CGI thead').remove();
    $('#datatable-CGI tbody').remove();
    
    $.ajax({
        method: 'POST',
        url: '../getCgi',
        success: function(response) {
            if(response.data == 0){
                
                $.ajax({
                    method: 'POST',
                    url: '../llenarSalaCgi',
                
                    success: function(response) {
                        $('.panel-body').show();
                        response['columnas'].push('Prioridad','Riesgo')
                        crearFilaTitulo(response['columnas'],"CGI");
                        crearTabla(response['data'],"CGI");
                        
                    }
                });

            }else{
                datos= response['data'];
                $('.panel-body').show();
                response['columnas'].push('Prioridad','Riesgo')
                crearFilaTitulo(response['columnas'],"CGI");
                crearTabla(response['data'],"CGI");
                
            }

            $('#datatable-CGI').dataTable({
                order: [ [7,'desc'],[8, 'desc']],
                pageLength: 5,
                bFilter: false,
                bInfo: false,
                bAutoWidth: false
            } );
            
        },
        done: function(){
            
        },
    });
}



function getSalaPediatria(){
    $('#datatable-Pediatria thead').remove();
    $('#datatable-Pediatria tbody').remove();
    
    $.ajax({
        method: 'POST',
        url: '../getPediatria',
        success: function(response) {
            if(response.data == 0){
                
                $.ajax({
                    method: 'POST',
                    url: '../llenarSalaPediatria',
                
                    success: function(response) {
                        $('.panel-body').show();
                        crearFilaTitulo(response['columnas'],"Pediatria");
                        crearTabla(response['data'],"Pediatria");
                        
                        
                    }
                });

            }else{
                datos= response['data'];
                $('.panel-body').show();
                response['columnas'].push('Prioridad','Riesgo')
                crearFilaTitulo(response['columnas'],"Pediatria");
                crearTabla(response['data'],"Pediatria");
            }

            $('#datatable-Pediatria').dataTable({
                order: [ [7,'desc'],[8, 'desc']],
                pageLength: 5,
                bFilter: false,
                bInfo: false,
                bAutoWidth: false
            } );
            
        },
        done: function(){
            
        },
    });
}