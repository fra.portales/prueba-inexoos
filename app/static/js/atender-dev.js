$(document).ready(function(){
    getListaAtencion();
    
});

function getListaAtencion(){
    $('#datatable-details thead').remove();
    $('#datatable-details tbody').remove();
    let response;
    let datos;
    response = callPacientesSinAtender();
    $('.panel-body').show();
    response['columnas'].push('Prioridad','Riesgo')
    datos = agrgarCalculos(response['data']);

    crearFilaTitulo(response['columnas'],"details");
    crearTabla(datos,"details");
    
}